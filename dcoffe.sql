--
-- File generated with SQLiteStudio v3.3.3 on �. �.�. 28 00:43:28 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
CREATE TABLE category (category_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, category_name TEXT (50) NOT NULL);
INSERT INTO category (category_id, category_name) VALUES (1, 'my coffee');
INSERT INTO category (category_id, category_name) VALUES (2, 'dessert');

-- Table: product
CREATE TABLE product (product_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, product_name TEXT (50) UNIQUE, product_price DOUBLE NOT NULL, product_size TEXT (5) DEFAULT SML NOT NULL, product_swtLevel TEXT (5) DEFAULT (123) NOT NULL, product_type TEXT (5) DEFAULT HCF NOT NULL, product_category INTEGER DEFAULT (1) NOT NULL, category_id INTEGER REFERENCES category (category_id) ON DELETE RESTRICT ON UPDATE RESTRICT NOT NULL DEFAULT (1));
INSERT INTO product (product_id, product_name, product_price, product_size, product_swtLevel, product_type, product_category, category_id) VALUES (1, 'Espresso', 30.0, 'SML', '0123', 'HCF', 1, 1);
INSERT INTO product (product_id, product_name, product_price, product_size, product_swtLevel, product_type, product_category, category_id) VALUES (2, 'Americano', 40.0, 'SML', '012', 'HC', 1, 1);
INSERT INTO product (product_id, product_name, product_price, product_size, product_swtLevel, product_type, product_category, category_id) VALUES (3, 'StrawberryCake', 50.0, '-', '-', '-', 2, 1);
INSERT INTO product (product_id, product_name, product_price, product_size, product_swtLevel, product_type, product_category, category_id) VALUES (4, 'Butter Cake', 60.0, '-', '-', '-', 2, 1);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
