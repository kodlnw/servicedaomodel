--
-- File generated with SQLiteStudio v3.3.3 on �. �.�. 28 00:43:17 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: customer
CREATE TABLE customer (
 Cus_id INTEGER PRIMARY KEY,
 Cus_name TEXT NOT NULL,
 Cus_PhoneNumber TEXT NOT NULL,
 Cus_Address TEXT NOT NULL
);

-- Table: Employee
CREATE TABLE Employee (
 EM_ID INTEGER PRIMARY KEY,
 EM_Firstname TEXT NOT NULL,
 EM_Position TEXT NOT NULL,
 EM_IP_Address TEXT NOT NULL,
 EM_Phone TEXT NOT NULL,
 EM_Salary INTEGER NOT NULL
);

-- Table: Order
CREATE TABLE "Order" (
 Order_ID INTEGER PRIMARY KEY,
 Order_Name TEXT NOT NULL,
 Order_Date TEXT NOT NULL,
 Order_Time TEXT NOT NULL,
 Order_Price INTEGER NOT NULL,
 Cus_id INTEGER NOT NULL,
 EM_ID INTEGER NOT NULL,
 Store_ID INTEGER NOT NULL,
 FOREIGN KEY("Cus_id") REFERENCES customer(Cus_id),
 FOREIGN KEY(EM_ID) REFERENCES Employee(EM_ID),
 FOREIGN KEY(Store_ID) REFERENCES Store(Store_ID)
);

-- Table: Order_item
CREATE TABLE Order_item (
 Order_item_ID INTEGER PRIMARY KEY,
 Order_item_Name TEXT NOT NULL,
 Order_item_Amount INTEGER NOT NULL,
 Order_item_Price INTEGER NOT NULL,
 Order_ID INTEGER NOT NULL,
 Product_ID INTEGER NOT NULL,
 FOREIGN KEY("Order_ID") REFERENCES "Order" ("Order_ID"),
 FOREIGN KEY ("Product_ID") REFERENCES "Product" (Product_ID)
);

-- Table: Product
CREATE TABLE Product (
 Product_ID INTEGER PRIMARY KEY,
 Product_Name TEXT NOT NULL,
 Product_Price INTEGER NOT NULL,
 Product_Level TEXT NOT NULL,
 Product_image BLOB  NOT NULL
);

-- Table: Store
CREATE TABLE Store (
 Store_ID INTEGER PRIMARY KEY,
 Store_name TEXT NOT NULL,
 Store_address TEXT NOT NULL,
 Store_tel TEXT NOT NULL,
 Store_tax TEXT NOT NULL,
 Store_logo BLOB NOT NULL
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
